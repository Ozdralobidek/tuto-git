tutoGit.md


# Git tuto 

git status : connaitre sur quelle branche on est et quelle action à faire (vue sur l’index)
git rm --cached « nom de doc » : virer des docs 
git init : initialiser un projet git
git commit : commiter
ouverture d’un editeur de texte ou on doit donner une info au commit
git log : graphe des commit
git add . : ajouter les docs à l’index
git add . && git commit –m « encore un commit » : push chainé
git restore « nom de doc » : restorer le commit avant
git commit –a : commit chainé
git commit --amend : changer le texte qu’il y avait dans le commit précédent
git add  . && git commit --amend : modifier un fichier du commit précédent
git switch : revenir sur le dernier commit
git rebase : changer le commit précédent
git log : pour avoir le SHA
git rebase –i <SHA> : avoir les pick
pick : une fois dans rebase selectionner le commit conserver
squash : fusionner avec le commit précédent
drop : supprimer à partir de ce commit
i : entrer en mode insertion
echap : quitter le mode insertion
:wq : enregistre et quitter
:aq : quitter sans enregistrer
:q : quitter

 
Exercice 1 :
-	Initialiser un projet git
-	Créer un fichier readme
-	Ajouter du texte dedans
-	Ajouter à l’index
-	Réaliser un commit
-	Vérifier l’index et l’historique
-	Réaliser une autre modification
-	Ajouter à l’index
-	Réaliser un deuxieme commit
-	Noter le sha du commit
-	Modifier le fichier
-	Modifier le dernier commit
-	Vérifier l’historique
-	Comparer le sha du commit


Exercice 2 : 
- Créer un commit avec comme message pick
- Créer un commit avec comme message squash
- Créer un commit avec comme message drop
- Créer un commit avec comme message pick




git checkout -b nom de la branche : pour créer une branche
git checkout nom de la branche : changer de branche
git merge nom de la branche : merger vers la branche
git branch -d nom de la branche : supprimer la branche 
git switch nom de la branche : switcher vers la branche 
git switch - : vers la branche du dernier commit

