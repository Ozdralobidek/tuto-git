Documentation Markdown (readme.md)
Les # permettent de faire des niveaux de titre, on peut en avoir jusqu’à 6.


Deux sauts de ligne pour aller à la ligne.


*Italique*


_Italique_


**Gras**


__Gras__


**Gras_Italique_**


# Listes combinées 

Liste de taches 
- Etapes à réaliser
1. étape 1
2. étape 2
    - sous étape

# Bloc code
'''C
int C;
C = 0;
'''

# Citation

> Une citation

Lien vers le site du [CESI](http://www.cesi.fr)

![Logo CESI](https://www.cesi.fr/wp-content/uploads/2022/07/lg_cesi.png "Logo CESI")

# Tableau

|Article | Quantite | Prix |
|--------|:-------:|-------|
| Banane | 3     | 1.00  |
|Pomme   | 4     | 2     |




# Ligne

----------------------
********
________
